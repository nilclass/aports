# Contributor: Konstantin Kulikov <k.kulikov2@gmail.com>
# Maintainer: Konstantin Kulikov <k.kulikov2@gmail.com>
pkgname=bpftrace
pkgver=0.9.5_git20200329
_sha=f087abbdd2e6108183887073b04718ee46dba36d
pkgrel=1
_llvmver=10
pkgdesc="High-level tracing language for Linux eBPF"
url="https://github.com/iovisor/bpftrace"
arch="x86_64"
license="Apache-2.0"
makedepends="cmake llvm$_llvmver-dev llvm$_llvmver-static clang-dev clang-static
	flex-dev bison elfutils-dev linux-headers bcc-dev binutils-dev"
source="$pkgname-$pkgver.zip::https://github.com/iovisor/bpftrace/archive/$_sha.zip"
# Tests require root, network to download gmock and a few tests fail.
options="!check"
builddir="$srcdir/$pkgname-$_sha/build"
subpackages="$pkgname-doc:doc $pkgname-tools:tools:noarch $pkgname-tools-doc:tools_doc"

prepare() {
	default_prepare
	mkdir $builddir
}

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING:BOOL=OFF \
		-DLLVM_REQUESTED_VERSION=$_llvmver \
		..
	make
}

# Main package contains only bpftrace binary.
package() {
	make DESTDIR="$pkgdir" install

	mv "$pkgdir/usr/share/bpftrace/tools/doc" "$pkgdir/usr/share/bpftrace/tools_doc"
}

# Doc package should contain only man page for bpftrace.
doc() {
	mkdir -p "$subpkgdir/usr/share/man/man8"
	mv "$pkgdir/usr/share/man/man8/bpftrace.8.gz" "$subpkgdir/usr/share/man/man8/"
}

# Tools are not installed in PATH, because they would conflict with other tools provided by bcc or perf-tools.
tools() {
	depends="$pkgname"
	pkgdesc="$pkgdesc (tools)"
	mkdir -p "$subpkgdir/usr/share/bpftrace"
	mv "$pkgdir/usr/share/bpftrace/tools" "$subpkgdir/usr/share/bpftrace/tools"
}

# Tools docs contains man pages and examples for tools. They shouldn't go into shared MANPATH for the same reasons as tools pkg above.
tools_doc() {
	pkgdesc="$pkgdesc (tool docs and examples)"

	mkdir -p "$subpkgdir/usr/share/bpftrace"
	mv "$pkgdir/usr/share/bpftrace/tools_doc" "$subpkgdir/usr/share/bpftrace/doc"
	mv "$pkgdir/usr/share/man/man8" "$subpkgdir/usr/share/bpftrace/"
}

sha512sums="edfaa606a40184a785525de2adb63d66e47e8d1c8bce67ed14809825dc40a6db342346fcfe4166ced272e2cf4884246f086ba35d68b601433213b025bc223395  bpftrace-0.9.5_git20200329.zip"
