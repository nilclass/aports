# Contributor: Adrian Siekierka <kontakt@asie.pl>
# Maintainer: Adrian Siekierka <kontakt@asie.pl>
pkgname=openttd
pkgver=1.10.0
pkgrel=0
pkgdesc="Open source version of the Transport Tycoon Deluxe simulator"
url="https://www.openttd.org"
arch="all"
license="GPL-2.0-or-later"
makedepends="fontconfig-dev freetype-dev icu-dev libpng-dev lzo-dev sdl2-dev xz-dev zlib-dev"
checkdepends="openttd-opengfx"
subpackages="$pkgname-doc $pkgname-lang::noarch"
source="https://cdn.openttd.org/openttd-releases/$pkgver/openttd-$pkgver-source.tar.xz"

build() {
	# OpenTTD's builtin strip does not work when cross-compiling,
	# but it will be done automatically later in the package build.
	./configure \
		--build="$CBUILD" \
		--host="$CHOST" \
		--disable-strip \
		--prefix-dir=/usr \
		--binary-dir=bin \
		--install-dir="$pkgdir" \
		--with-sdl \
		--with-zlib \
		--with-liblzma \
		--with-liblzo2 \
		--with-freetype \
		--with-fontconfig \
		--without-icu
	make
}

check() {
	make test
}

package() {
	make install
}

lang() {
	pkgdesc="$pkgdesc (localizations)"

	cd "$pkgdir"
	mkdir -p "$subpkgdir"/usr/share/games/openttd/lang

	local i
	for i in $(find usr/share/games/openttd/lang/ ! -name 'english.lng' -type f); do
		mv "$i" "$subpkgdir"/"$i"
	done
}

sha512sums="f1727b81a059aa04bea1fb6a9a89ae0619942d6406a77f99e642a60ea9416daa215bbd0cbd183747c2df5f6dea81766b7e04493a52211e55522e8b2642db701a  openttd-1.10.0-source.tar.xz"
