# Contributor: Eivind Uggedal <eu@eju.no>
# Maintainer: Sören Tempel <soeren+alpinelinux@soeren-tempel.net>
pkgname=mksh
pkgver=58
_pkgver=R$pkgver
pkgrel=0
pkgdesc="MirBSD KSH Shell"
url="https://www.mirbsd.org/mksh"
arch="all"
license="BSD"
depends=""
depends_dev=""
makedepends=""
checkdepends="perl"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.pre-deinstall"
subpackages="$pkgname-doc"
source="https://www.mirbsd.org/MirOS/dist/mir/$pkgname/$pkgname-$_pkgver.tgz"
builddir="$srcdir/$pkgname"

prepare() {
	default_prepare

	# This test case checks if a controlling tty is not present when
	# `-C regress:no-ctty` has been passed. However, a controlling
	# tty might be present when building this APKBUILD locally but
	# isn't present on the builder. To make sure the tests pass both
	# locally and on the builders we remove this test case.
	sed -i "$builddir"/check.t -e '/^name: selftest-tty-absent$/,/^---$/d'
}

build() {
	cd "$builddir"
	sh Build.sh -r
}

check() {
	cd "$builddir"
	./test.sh -v -f -C regress:no-ctty
}

package() {
	cd "$builddir"
	install -Dm755 mksh "$pkgdir"/bin/mksh
	install -Dm644 mksh.1 "$pkgdir"/usr/share/man/man1/mksh.1
	install -Dm644 dot.mkshrc "$pkgdir"/usr/share/doc/mksh/dot.mkshrc
}

sha512sums="711351f8bbe8e44fcf9e7963f8e749938ec3ccb362fafd5350d44593841c2acb6d54ffa115dbb6b83c30865728a4c5274c05feedb063e293361e2830d263d80f  mksh-R58.tgz"
