# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Michael Mason <ms13sp@gmail.com>
pkgname=freetds
pkgver=1.1.26
pkgrel=0
pkgdesc="Tabular Datastream Library"
url="https://www.freetds.org"
arch="all"
license="GPL-2.0-or-later OR LGPL-2.0-or-later"
makedepends="openssl-dev linux-headers readline-dev unixodbc-dev"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
source="https://www.freetds.org/files/stable/freetds-$pkgver.tar.bz2
	fix-includes.patch
	"
builddir="$srcdir/$pkgname-$pkgver"
options="!check"  # tests require running SQL server http://www.freetds.org/userguide/confirminstall.htm#TESTS

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-msdblib \
		--with-openssl=/usr \
		--enable-odbc \
		--with-unixodbc=/usr
	make
}

check() {
	cd "$builddir"/src/replacements
	make check
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="cd3eb738eaa6793afdeaf09a077310c4d6d8a4bd24f3bd3a796654cf322bd055e22865d0fd8d9a537957b808eabb6891162048d0d8f7f2097720d3a554548e6a  freetds-1.1.26.tar.bz2
d75d1aab6687586697f3e430db1e82f21208f10076b45996542eea682e36cbbbb344f479a9336fcfd294b5b87d7acb2ec5fb8ddd1914e990e23dd5e7ae93a0b6  fix-includes.patch"
