# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=gns3-gui
pkgver=2.2.6
pkgrel=0
pkgdesc="GNS3 network simulator. Graphical user interface package."
url="https://github.com/GNS3/gns3-gui"
arch="noarch !armhf" # armhf blocked by py3-qt5 -> qt5-qtdeclarative
license="GPL-3.0-or-later"
depends="python3 py3-psutil-gns3 py3-jsonschema-gns3 py3-raven
	py3-qt5 qt5-qtsvg py3-sip"
makedepends="python3-dev py3-setuptools"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/GNS3/gns3-gui/archive/v$pkgver.tar.gz"

build() {
	python3 setup.py build
}

check() {
	python3 setup.py check
}

package() {
	python3 setup.py install --root="$pkgdir" --optimize=1
	install -Dm644 "$builddir"/resources/images/gns3_icon_256x256.png "$pkgdir"/usr/share/pixmaps/gns3.png
	install -Dm644 "$builddir"/LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="193c132102c2c3467859053187627a7b1d1920de2fe52a4eaa2ccc264512b58e428bba144af5b3217efdfbfb20107081871a4e5f8cd220cacb4ab4d005eb2aef  gns3-gui-2.2.6.tar.gz"
