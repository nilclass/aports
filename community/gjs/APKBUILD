# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gjs
pkgver=1.64.1
pkgrel=0
pkgdesc="GNOME javascript library"
url="https://wiki.gnome.org/Projects/Gjs"
arch="all !s390x !mips !mips64"
license="MIT AND LGPL-2.0-or-later"
makedepends="dbus gobject-introspection-dev mozjs68-dev mozjs68
	gtk+3.0-dev cairo-dev meson"
checkdepends="xvfb-run"
subpackages="$pkgname-dev"
source="https://download.gnome.org/sources/gjs/${pkgver%.*}/gjs-$pkgver.tar.xz"

# Tests fail on 32-bit: https://gitlab.gnome.org/GNOME/gjs/-/issues/312
case "$CARCH" in
	x86|armv7|armhf) options="!check";;
esac

build() {
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		-Dprofile=disabled \
		-Dinstalled_tests=false \
		output
	ninja -C output
}

check() {
	xvfb-run ninja -C output test
}

package() {
	DESTDIR="$pkgdir" ninja -C output install
}

sha512sums="8d076b9d9ff3eb66dc76ff1f3ea4c24de0604979b537cd09a62e3c5718f08a0acb6f29de60ed62bb7215a8ffa1387904db98e147d9b20cfab0391f7060566b2f  gjs-1.64.1.tar.xz"
